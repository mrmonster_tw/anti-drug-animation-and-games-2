﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class screenshot : MonoBehaviour {

	private RenderTexture TargetTexture;
    public GameObject UIS;
    public GameObject but;
    public GameObject hint;
    public GameObject exit;
    public GameObject word;
    public GameObject color;
    public GameObject enter;

    public GameObject inputword;
    public GameObject can;

    public GameObject nowwork;
    public Text worktext;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
       
    }
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
         TargetTexture = source;
         Graphics.Blit(source, destination);
    }
 
    private void Save()
    {
         RenderTexture.active = TargetTexture;
 
         Texture2D png = new Texture2D(RenderTexture.active.width, RenderTexture.active.height, TextureFormat.ARGB32, true);
         png.ReadPixels(new Rect(0, 0, RenderTexture.active.width, RenderTexture.active.height), 0, 0);
 
         byte[] bytes = png.EncodeToPNG();
         string path = string.Format(@"D:\halloffame.png");
         FileStream file = File.Open(path, FileMode.Create);
         BinaryWriter writer = new BinaryWriter(file);
         writer.Write(bytes);
         file.Close();
         writer.Close();
 
         Destroy(png);
         png = null;
         
         Debug.Log("Save Down");
    }
	public void take_shot()
	{
		UIS.SetActive (false);
        StartCoroutine (wait_show());
    }
	private IEnumerator wait_show()
	{
		yield return new WaitForSeconds (0.5f);
		Save();
		yield return new WaitForSeconds (1f);
        UIS.SetActive(true);
        hint.SetActive(true);
    }
    public void close_hint()
    {
        hint.SetActive(false);
    }
    public void exit_fn()
    {
        Application.Quit();
    }
    public void word_fn()
    {
        word.SetActive(false);
        color.SetActive(true);
        enter.SetActive(true);
        GameObject word_clone = Instantiate(inputword, can.transform);
        word_clone.transform.localPosition = new Vector3(0, 0, 0);
        nowwork = word_clone;
        worktext = nowwork.GetComponent<InputField>().textComponent;
    }
    public void color_fn(int xx)
    {
        if (xx == 1)
        {
            worktext.GetComponent<Text>().color = new Color32(75,94,153,255);
        }
        if (xx == 2)
        {
            worktext.GetComponent<Text>().color = new Color32(79, 79, 79, 255);
        }
        if (xx == 3)
        {
            worktext.GetComponent<Text>().color = new Color32(70, 143, 188, 255);
        }
        if (xx == 4)
        {
            worktext.GetComponent<Text>().color = new Color32(83, 132, 117, 255);
        }
        if (xx == 5)
        {
            worktext.GetComponent<Text>().color = new Color32(198, 102, 141, 255);
        }
        if (xx == 6)
        {
            worktext.GetComponent<Text>().color = new Color32(221, 221, 221, 255);
        }
    }
    public void wordsize_fn(int xx)
    {
        if (xx == 1)
        {
            worktext.GetComponent<Text>().fontSize = 80;
        }
        if (xx == 2)
        {
            worktext.GetComponent<Text>().fontSize = 60;
        }
        if (xx == 3)
        {
            worktext.GetComponent<Text>().fontSize = 40;
        }
    }
    public void enter_fn()
    {
        nowwork.GetComponent<InputField>().enabled = false;
        word.SetActive(true);
        color.SetActive(false);
        enter.SetActive(false);
    }
    public void rotate_fn()
    {
        
    }
}
