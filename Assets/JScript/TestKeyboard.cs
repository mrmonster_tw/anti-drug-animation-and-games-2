﻿using UnityEngine;
using System.Collections;

public class TestKeyboard : MonoBehaviour
{
    public Vector2 range = new Vector2(50f, 50f);
    Transform mTrans;
    Quaternion mStart;
    Vector2 mRot = Vector2.zero;

    float halfWidth;
    float halfHeight;
    float x;
    float y;

    public bool rot;

    void Start()
    {
        mTrans = transform;
        mStart = mTrans.localRotation;
    }
    void Update()
    {
        if (rot)
        {
            Vector3 pos = Input.mousePosition;

            halfWidth = Screen.width * 0.5f;
            halfHeight = Screen.height * 0.5f;

            x = Mathf.Clamp((pos.x - halfWidth) / halfWidth, -1f, 1f);
            y = Mathf.Clamp((pos.y - halfHeight) / halfHeight, -1f, 1f);
            mRot = Vector2.Lerp(mRot, new Vector2(x, y), Time.deltaTime * 5f);
            //mTrans.localRotation = mStart * Quaternion.Euler(-mRot.y * range.y, mRot.x * range.x, 0f);
            mTrans.localRotation = mStart * Quaternion.Euler(0, 0, -mRot.y * range.y);
        }
    }
    public void rotate()
    {
        rot = true;
    }
}
