﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bar : MonoBehaviour
{
    public SpriteRenderer slot11;
    public SpriteRenderer slot12;
    public SpriteRenderer slot13;
    public SpriteRenderer slot21;
    public SpriteRenderer slot22;
    public SpriteRenderer slot23;
    public SpriteRenderer slot24;
    public SpriteRenderer slot25;
    public SpriteRenderer slot26;
    public SpriteRenderer slot27;
    public SpriteRenderer slot31;
    public SpriteRenderer slot32;
    public SpriteRenderer slot33;
    public SpriteRenderer slot34;
    public SpriteRenderer slot41;
    public SpriteRenderer slot42;
    public SpriteRenderer slot43;
    public SpriteRenderer slot44;
    public SpriteRenderer slot45;
    public SpriteRenderer slot51;
    public SpriteRenderer slot52;
    public SpriteRenderer slot53;
    public SpriteRenderer slot54;
    public SpriteRenderer slot55;
    public SpriteRenderer slot56;
    public SpriteRenderer slot57;
    public SpriteRenderer slot58;

    float speed_1;
    float speed_2;
    float speed_3;
    float speed_4;
    float speed_5;

    public GameObject slot_1;
    public GameObject slot_2;
    public GameObject slot_3;
    public GameObject slot_4;
    public GameObject slot_5;

    public bool go;
    public bool go_1;
    public bool go_2;
    public bool go_3;
    public bool go_4;
    public bool go_5;
    public bool stop;

    public int once;
    public GameObject click;
    public AudioSource roll;
    public AudioSource coin;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && go_1 == false && go_2 == false && go_3 == false && go_4 == false && go_5 == false && once == 0)
        {
            go = true;
            click.SetActive(false);
            roll.Play();
            coin.Play();

            speed_1 = Random.RandomRange(20, 30);
            speed_2 = Random.RandomRange(20, 30);
            speed_3 = Random.RandomRange(20, 30);
            speed_4 = Random.RandomRange(20, 30);
            speed_5 = Random.RandomRange(20, 30);
            GetComponent<Animator>().Play("bar");

            StartCoroutine(stoproll_fn());
            once = 1;
        }
        /*if (Input.GetMouseButtonDown(1))
        {
            go = false;
            stop = true;
        }*/
        if (go)
        {
            go_1 = true;
            go_2 = true;
            go_3 = true;
            go_4 = true;
            go_5 = true;
        }
       
        if (go_1 == false && go_2 == false && go_3 == false && go_4 == false && go_5 == false && once == 1)
        {
            stop = false;
            click.SetActive(true);
            roll.Stop();
            once = 0;
        }
        if (stop)
        {
            StartCoroutine(stopbystep());
            //stop = false;
        }
        if (go_1)
        {
            slot11.transform.Translate(Vector3.down * speed_1 * Time.deltaTime);
            slot12.transform.Translate(Vector3.down * speed_1 * Time.deltaTime);
            slot13.transform.Translate(Vector3.down * speed_1 * Time.deltaTime);
        }
        if (go_2)
        {
            slot21.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot22.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot23.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot24.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot25.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot26.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
            slot27.transform.Translate(Vector3.down * speed_2 * Time.deltaTime);
        }
        if (go_3)
        {
            slot31.transform.Translate(Vector3.down * speed_3 * Time.deltaTime);
            slot32.transform.Translate(Vector3.down * speed_3 * Time.deltaTime);
            slot33.transform.Translate(Vector3.down * speed_3 * Time.deltaTime);
            slot34.transform.Translate(Vector3.down * speed_3 * Time.deltaTime);
        }
        if (go_4)
        {
            slot41.transform.Translate(Vector3.down * speed_4 * Time.deltaTime);
            slot42.transform.Translate(Vector3.down * speed_4 * Time.deltaTime);
            slot43.transform.Translate(Vector3.down * speed_4 * Time.deltaTime);
            slot44.transform.Translate(Vector3.down * speed_4 * Time.deltaTime);
            slot45.transform.Translate(Vector3.down * speed_4 * Time.deltaTime);
        }
        if (go_5)
        {
            slot51.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot52.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot53.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot54.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot55.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot56.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot57.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
            slot58.transform.Translate(Vector3.down * speed_5 * Time.deltaTime);
        }
        restart_fn(slot11, slot12);
        restart_fn(slot12, slot13);
        restart_fn(slot13, slot11);
        restart_fn(slot21, slot22);
        restart_fn(slot22, slot23);
        restart_fn(slot23, slot24);
        restart_fn(slot24, slot25);
        restart_fn(slot25, slot26);
        restart_fn(slot26, slot27);
        restart_fn(slot27, slot21);
        restart_fn(slot31, slot32);
        restart_fn(slot32, slot33);
        restart_fn(slot33, slot34);
        restart_fn(slot34, slot31);
        restart_fn(slot41, slot42);
        restart_fn(slot42, slot43);
        restart_fn(slot43, slot44);
        restart_fn(slot44, slot45);
        restart_fn(slot45, slot41);
        restart_fn(slot51, slot52);
        restart_fn(slot52, slot53);
        restart_fn(slot53, slot54);
        restart_fn(slot54, slot55);
        restart_fn(slot55, slot56);
        restart_fn(slot56, slot57);
        restart_fn(slot57, slot58);
        restart_fn(slot58, slot51);
    }
    void restart_fn(SpriteRenderer xx, SpriteRenderer yy)
    {
        if (xx.transform.localPosition.y <= -6)
        {
            xx.transform.localPosition = new Vector3(xx.transform.localPosition.x, yy.transform.localPosition.y + 4.3f, xx.transform.localPosition.z);
        }
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    private IEnumerator stopbystep()
    {
        if (slot_1.GetComponent<position>().hit != null && slot_1.GetComponent<position>().hit.transform.localPosition.y <= 0)
        {
            go_1 = false;
            slot_1.GetComponent<position>().hit.transform.localPosition = new Vector3(slot_1.GetComponent<position>().hit.transform.localPosition.x, 0.38f, slot_1.GetComponent<position>().hit.transform.localPosition.z);
            slot_1.GetComponent<position>().hit.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        yield return new WaitForSeconds(0.5f);
        if (slot_2.GetComponent<position>().hit != null && slot_2.GetComponent<position>().hit.transform.localPosition.y <= 0)
        {
            go_2 = false;
            slot_2.GetComponent<position>().hit.transform.localPosition = new Vector3(slot_2.GetComponent<position>().hit.transform.localPosition.x, 0.38f, slot_2.GetComponent<position>().hit.transform.localPosition.z);
            slot_2.GetComponent<position>().hit.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        yield return new WaitForSeconds(0.5f);
        if (slot_3.GetComponent<position>().hit != null && slot_3.GetComponent<position>().hit.transform.localPosition.y <= 0)
        {
            go_3 = false;
            slot_3.GetComponent<position>().hit.transform.localPosition = new Vector3(slot_3.GetComponent<position>().hit.transform.localPosition.x, 0.38f, slot_3.GetComponent<position>().hit.transform.localPosition.z);
            slot_3.GetComponent<position>().hit.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        yield return new WaitForSeconds(0.5f);
        if (slot_4.GetComponent<position>().hit != null && slot_4.GetComponent<position>().hit.transform.localPosition.y <= 0)
        {
            go_4 = false;
            slot_4.GetComponent<position>().hit.transform.localPosition = new Vector3(slot_4.GetComponent<position>().hit.transform.localPosition.x, 0.38f, slot_4.GetComponent<position>().hit.transform.localPosition.z);
            slot_4.GetComponent<position>().hit.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
        yield return new WaitForSeconds(0.5f);
        if (slot_5.GetComponent<position>().hit != null && slot_5.GetComponent<position>().hit.transform.localPosition.y <= 0)
        {
            go_5 = false;
            slot_5.GetComponent<position>().hit.transform.localPosition = new Vector3(slot_5.GetComponent<position>().hit.transform.localPosition.x, 0.38f, slot_5.GetComponent<position>().hit.transform.localPosition.z);
            slot_5.GetComponent<position>().hit.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }
    private IEnumerator stoproll_fn()
    {
        yield return new WaitForSeconds(1);
        go = false;
        stop = true;
    }
}
