﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class bigview : MonoBehaviour
{
    public GameObject bview;
    public GameObject cyb;

    public int count;
    public Text counttxt;

    int ranchange;
    public GameObject ob_1;
    public GameObject ob_2;
    public GameObject ob_3;
    public GameObject win;
    public GameObject retart;

    public AudioSource error;

    // Start is called before the first frame update
    void Start()
    {
        ran_bg();
    }

    // Update is called once per frame
    void Update()
    {
        counttxt.text = count.ToString();
        if (count == 5)
        {
            win.SetActive(true);
            retart.SetActive(true);
        }
        //放大鏡
        bview.transform.localPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
        bview.transform.localPosition = new Vector3(bview.transform.localPosition.x, bview.transform.localPosition.y, -5);

        cyb.transform.localPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
        cyb.transform.localPosition = new Vector3(bview.transform.localPosition.x, bview.transform.localPosition.y, -5);

        if (Input.GetMouseButtonDown(0))
        {
            bview.SetActive(false);
            cyb.SetActive(false);
        }
    }
    public void bview_fn()
    {
        bview.SetActive(true);
        cyb.SetActive(true);
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    public void restart_fn()
    {
        SceneManager.LoadScene("red");
    }
    public void ran_bg()
    {
        ranchange = Random.Range(0, 3);
        if (ranchange == 0)
        {
            ob_1.SetActive(true);
        }
        if (ranchange == 1)
        {
            ob_2.SetActive(true);
        }
        if (ranchange == 2)
        {
            ob_3.SetActive(true);
        }
    }
}
