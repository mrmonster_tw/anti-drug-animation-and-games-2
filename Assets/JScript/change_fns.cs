﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change_fns : MonoBehaviour
{
    public SpriteRenderer eye;
    public SpriteRenderer hair;
    public SpriteRenderer nose;
    public SpriteRenderer mouth;
    public SpriteRenderer Rarm;
    public SpriteRenderer Larm;
    public SpriteRenderer cloth;
    public SpriteRenderer glass;
    public GameObject pinhole;
    public GameObject tatoo;
    public GameObject leftarm;

    public Sprite eye_1;
    public Sprite hair_1;
    public Sprite nose_1;
    public Sprite mouth_1;
    public Sprite cloth_1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void erase_fn()
    {
        eye.sprite = eye_1;
        hair.sprite = hair_1;
        nose.sprite = nose_1;
        mouth.sprite = mouth_1;
        //Rarm.sprite = null;
        //Larm.sprite = null;
        cloth.sprite = cloth_1;
        glass.sprite = null;
        pinhole.SetActive(false);
        tatoo.SetActive(false);
        leftarm.SetActive(false);
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    public void pinhole_fn()
    {
        if (pinhole.activeSelf == false)
        {
            pinhole.SetActive(true);
            return;
        }
        if (pinhole.activeSelf)
        {
            pinhole.SetActive(false);
        }
    }
    public void tatoo_fn()
    {
        if (tatoo.activeSelf == false)
        {
            tatoo.SetActive(true);
            return;
        }
        if (tatoo.activeSelf)
        {
            tatoo.SetActive(false);
        }
    }
    public void tatoo1_fn()
    {
        if (leftarm.activeSelf == false)
        {
            leftarm.SetActive(true);
            return;
        }
        if (leftarm.activeSelf)
        {
            leftarm.SetActive(false);
        }
    }
}
