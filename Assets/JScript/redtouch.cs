﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redtouch : MonoBehaviour
{
    public Sprite right;
    public GameObject gm;
    public bool check;
    public AudioSource correct;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnMouseDown()
    {
        if (check == false)
        {
            this.GetComponent<SpriteRenderer>().sprite = right;
            gm.GetComponent<bigview>().count++;
            check = true;
            correct.Play();
        }
    }
}
