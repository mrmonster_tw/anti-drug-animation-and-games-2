﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GM : MonoBehaviour
{
    public GameObject card_1;
    public GameObject card_2;
    public GameObject card_3;
    public GameObject card_4;
    public GameObject card_5;
    public GameObject card_6;
    public GameObject card_7;
    public GameObject card_8;
    public GameObject card_9;
    public GameObject card_10;
    public GameObject card_11;
    public GameObject card_12;
    public GameObject card_13;
    public GameObject card_14;
    public GameObject card_15;
    public GameObject card_16;
    public GameObject card_17;
    public GameObject card_18;
    public GameObject card_19;
    public GameObject card_20;
    public Sprite card_back;
    public GameObject clear;
    public GameObject win;
    int once_1;

    Vector3 pos_1;
     Vector3 pos_2;
     Vector3 pos_3;
     Vector3 pos_4;
     Vector3 pos_5;
     Vector3 pos_6;
     Vector3 pos_7;
     Vector3 pos_8;
     Vector3 pos_9;
     Vector3 pos_10;
     Vector3 pos_11;
     Vector3 pos_12;
     Vector3 pos_13;
     Vector3 pos_14;
     Vector3 pos_15;
     Vector3 pos_16;
     Vector3 pos_17;
     Vector3 pos_18;
     Vector3 pos_19;
     Vector3 pos_20;
    public GameObject evesys;
    int once;

    int rw;
    public bool bingo;
    public bool wrong;
    public GameObject[] cards = new GameObject[20];
    public List<GameObject> picks = new List<GameObject>();

    public AudioSource right;
    public AudioSource error;

    public GameObject check;
    public GameObject block;

    // Start is called before the first frame update
    void Start()
    {
        for (int x = 0; x < 20; x++)
        {
            pic_gb(x);
            if (x > 0)
            {
                ran_dom(x);
            }
        }

        pos_1 = card_1.transform.localPosition;
        pos_2 = card_2.transform.localPosition;
        pos_3 = card_3.transform.localPosition;
        pos_4 = card_4.transform.localPosition;
        pos_5 = card_5.transform.localPosition;
        pos_6 = card_6.transform.localPosition;
        pos_7 = card_7.transform.localPosition;
        pos_8 = card_8.transform.localPosition;
        pos_9 = card_9.transform.localPosition;
        pos_10 = card_10.transform.localPosition;
        pos_11 = card_11.transform.localPosition;
        pos_12 = card_12.transform.localPosition;
        pos_13 = card_13.transform.localPosition;
        pos_14 = card_14.transform.localPosition;
        pos_15 = card_15.transform.localPosition;
        pos_16 = card_16.transform.localPosition;
        pos_17 = card_17.transform.localPosition;
        pos_18 = card_18.transform.localPosition;
        pos_19 = card_19.transform.localPosition;
        pos_20 = card_20.transform.localPosition;

        cards[0].transform.localPosition = pos_1;
        cards[1].transform.localPosition = pos_2;
        cards[2].transform.localPosition = pos_3;
        cards[3].transform.localPosition = pos_4;
        cards[4].transform.localPosition = pos_5;
        cards[5].transform.localPosition = pos_6;
        cards[6].transform.localPosition = pos_7;
        cards[7].transform.localPosition = pos_8;
        cards[8].transform.localPosition = pos_9;
        cards[9].transform.localPosition = pos_10;
        cards[10].transform.localPosition = pos_11;
        cards[11].transform.localPosition = pos_12;
        cards[12].transform.localPosition = pos_13;
        cards[13].transform.localPosition = pos_14;
        cards[14].transform.localPosition = pos_15;
        cards[15].transform.localPosition = pos_16;
        cards[16].transform.localPosition = pos_17;
        cards[17].transform.localPosition = pos_18;
        cards[18].transform.localPosition = pos_19;
        cards[19].transform.localPosition = pos_20;
    }

    // Update is called once per frame
    void Update()
    {
        if (card_1 == null && card_2 == null && card_3 == null && card_4 == null && card_5 == null && card_6 == null && card_7 == null && card_8 == null && card_9 == null && card_10 == null && card_11 == null && card_12 == null && card_13 == null && card_14 == null && card_15 == null && card_16 == null && card_17 == null && card_18 == null && card_19 == null && card_20 == null && once_1 == 0)
        {
            clear.SetActive(true);
            win.SetActive(true);
            once_1 = 1;
        }
        if (picks.Count == 2 && once == 0)
        {
            if (picks[0].name == picks[1].name)
            {
                bingo = true;
                //evesys.SetActive(false);
                //StartCoroutine(success_fn());
                check.SetActive(true);
                block.SetActive(true);
                right.Play();
            }
            if (picks[0].name != picks[1].name)
            {
                wrong = true;
                evesys.SetActive(false);
                StartCoroutine(fail_fn());
                error.Play();
            }
            once = 1;
        }
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    public void restart_fn()
    {
        SceneManager.LoadScene("card");
    }
    private IEnumerator success_fn()
    {
        yield return new WaitForSeconds(2);
        Destroy(picks[0]);
        Destroy(picks[1]);

        wrong = false;
        bingo = false;
        evesys.SetActive(true);
        picks.Clear();
        once = 0;
    }
    public void check_fn()
    {
        Destroy(picks[0]);
        Destroy(picks[1]);

        check.SetActive(false);
        block.SetActive(false);
        wrong = false;
        bingo = false;
        evesys.SetActive(true);
        picks.Clear();
        once = 0;
    }
    private IEnumerator fail_fn()
    {
        yield return new WaitForSeconds(2);
        wrong = false;
        bingo = false;
        evesys.SetActive(true);

        picks[0].GetComponent<Image>().sprite = card_back;
        picks[1].GetComponent<Image>().sprite = card_back;
        picks[0].GetComponent<Card>().chosed = false;
        picks[1].GetComponent<Card>().chosed = false;

        picks.Clear();
        once = 0;
    }

    void ran_dom(int i)
    {
        for (int y = 0; y < i;)
        {
            if (cards[i] == cards[y])
            {
                pic_gb(i);
                y = 0;
            }
            else
            {
                y++;
            }
        }
    }
    void pic_gb(int u)
    {
        rw = Random.RandomRange(1, 21);
        if (rw == 1)
        {
            cards[u] = card_1;
        }
        if (rw == 2)
        {
            cards[u] = card_2;
        }
        if (rw == 3)
        {
            cards[u] = card_3;
        }
        if (rw == 4)
        {
            cards[u] = card_4;
        }
        if (rw == 5)
        {
            cards[u] = card_5;
        }
        if (rw == 6)
        {
            cards[u] = card_6;
        }
        if (rw == 7)
        {
            cards[u] = card_7;
        }
        if (rw == 8)
        {
            cards[u] = card_8;
        }
        if (rw == 9)
        {
            cards[u] = card_9;
        }
        if (rw == 10)
        {
            cards[u] = card_10;
        }
        if (rw == 11)
        {
            cards[u] = card_11;
        }
        if (rw == 12)
        {
            cards[u] = card_12;
        }
        if (rw == 13)
        {
            cards[u] = card_13;
        }
        if (rw == 14)
        {
            cards[u] = card_14;
        }
        if (rw == 15)
        {
            cards[u] = card_15;
        }
        if (rw == 16)
        {
            cards[u] = card_16;
        }
        if (rw == 17)
        {
            cards[u] = card_17;
        }
        if (rw == 18)
        {
            cards[u] = card_18;
        }
        if (rw == 19)
        {
            cards[u] = card_19;
        }
        if (rw == 20)
        {
            cards[u] = card_20;
        }
    }
}
