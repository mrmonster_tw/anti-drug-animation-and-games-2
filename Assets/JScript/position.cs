﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class position : MonoBehaviour
{
    public GameObject hit;
    public GameObject gm;
    public bool stp;
    public int once;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.GetComponent<bar>().go && once == 1)
        {
            hit.GetComponent<SpriteRenderer>().sortingOrder = 0;
            hit = null;
            stp = false;
            once = 0;
        }
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "stop" && gm.GetComponent<bar>().stop && once == 0)
        {
            hit = col.gameObject;
            stp = true;
            once = 1;
        }
    }
}
