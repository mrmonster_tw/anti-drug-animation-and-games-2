﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class drag : MonoBehaviour
{
    float OffsetX;
    float OffsetY;



    public Vector2 range = new Vector2(50f, 50f);
    Transform mTrans;
    Quaternion mStart;
    Vector2 mRot = Vector2.zero;

    float halfWidth;
    float halfHeight;
    float x;
    float y;

    public bool rb;
    public GameObject rotspins;

    // Start is called before the first frame update
    void Start()
    {
        mTrans = transform;
        mStart = mTrans.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, 10, -1); ;
        if (hit.collider)
        {
            hit.transform.gameObject.GetComponent<Image>().enabled = true;
            Debug.Log(hit.transform.name);
        }
        else
        {
            rotspins.GetComponent<Image>().enabled = false;
        }

        if (rb)
        {
            Vector3 pos = Input.mousePosition;

            halfWidth = Screen.width * 0.5f;
            halfHeight = Screen.height * 0.5f;

            x = Mathf.Clamp((pos.x - halfWidth) / halfWidth, -1f, 1f);
            y = Mathf.Clamp((pos.y - halfHeight) / halfHeight, -1f, 1f);
            mRot = Vector2.Lerp(mRot, new Vector2(x, y), Time.deltaTime * 5f);
            //mTrans.localRotation = mStart * Quaternion.Euler(-mRot.y * range.y, mRot.x * range.x, 0f);
            mTrans.localRotation = mStart * Quaternion.Euler(0, 0, -mRot.y * range.y);

            if (Input.GetMouseButtonDown(0))
            {
                rb = false;
            }
        }
    }
    public void BeginDrag()
    {
        OffsetX = transform.localPosition.x - Input.mousePosition.x;
        OffsetY = transform.localPosition.y - Input.mousePosition.y;
    }
    public void OnDrag()
    {
        this.GetComponent<Image>().enabled = true;
        transform.localPosition = new Vector3(Input.mousePosition.x + OffsetX, Input.mousePosition.y + OffsetY);
    }
    public void EndDrag()
    {
        this.GetComponent<Image>().enabled = false;
    }
    public void rotspin_fn()
    {
        rb = true;
    }
}
